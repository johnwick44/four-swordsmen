/*
MySQL Data Transfer
Source Host: localhost
Source Database: db_gongwenmm_g
Target Host: localhost
Target Database: db_gongwenmm_g
Date: 2021-12-9 13:18:05
*/

SET FOREIGN_KEY_CHECKS = 0;
-- ----------------------------
-- Table structure for t_admin
-- ----------------------------
CREATE TABLE `t_admin` (
  `userId` int(11) NOT NULL auto_increment,
  `userName` varchar(50) default NULL,
  `userPw` varchar(50) default NULL,
  PRIMARY KEY  (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_gaofei
-- ----------------------------
CREATE TABLE `t_gaofei` (
  `id` int(11) NOT NULL auto_increment,
  `jine` int(11) default NULL,
  `shijian` varchar(111) default NULL,
  `user_id` int(11) default NULL,
  `del` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_gaojian
-- ----------------------------
CREATE TABLE `t_gaojian` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(50) default NULL,
  `fujian` varchar(50) default NULL,
  `fujianYuanshiming` varchar(50) default NULL,
  `shijian` varchar(50) default NULL,
  `user_id` int(11) default NULL,
  `zhuanjiashenhebiaozhi` varchar(255) default NULL,
  `zhuanjiashenheyijian` varchar(5000) default NULL,
  `zhubianshenhebiaozhi` varchar(50) default NULL,
  `zhubianshenheyijian` varchar(5000) default NULL,
  `touser` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `sex` varchar(50) default NULL,
  `age` varchar(50) default NULL,
  `tel` varchar(50) default NULL,
  `address` varchar(50) default NULL,
  `loginName` varchar(50) default NULL,
  `loginPw` varchar(50) default NULL,
  `del` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_zhuanjia
-- ----------------------------
CREATE TABLE `t_zhuanjia` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `sex` varchar(50) default NULL,
  `age` varchar(50) default NULL,
  `tel` varchar(50) default NULL,
  `address` varchar(50) default NULL,
  `loginName` varchar(50) default NULL,
  `loginPw` varchar(50) default NULL,
  `del` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `t_admin` VALUES ('1', 'admin', 'admin');
INSERT INTO `t_gaofei` VALUES ('7', '100', '2017-06-01', '2', 'no');
INSERT INTO `t_gaofei` VALUES ('8', '100', '2017-06-07', '2', 'no');
INSERT INTO `t_gaofei` VALUES ('9', '100', '2017-06-07', '2', 'no');
INSERT INTO `t_gaojian` VALUES ('17', '北京电子科技学院关于商洽2021年寒假期间我院师生支教活动具体事宜的函', '/upload/1638760921154.pdf', '函.pdf', '2021-12-06 11:21', '1', '通过', '好！', '通过', '好！', '2');
INSERT INTO `t_gaojian` VALUES ('18', 'X 省 X 县教育局关于北京电子科技学院商洽寒假期间支教工作事宜的复函', '/upload/1638761084599.pdf', '复函.pdf', '2021-12-06 11:24', '2', '通过', '1', '审核中', '', '1');
INSERT INTO `t_gaojian` VALUES ('19', '111', '/upload/1638978517248.pdf', '复函.pdf', '2021-12-08 23:48', '1', '通过', '111', '通过', '11', 'zhangwenying');
INSERT INTO `t_user` VALUES ('1', '李杰', '女', '20', '000-0000000', '01000000', 'lj', 'lj', 'no');
INSERT INTO `t_user` VALUES ('2', '任家萱', '女', '20', '000-0000000', '01000000', 'rjx', 'rjx', 'no');
INSERT INTO `t_zhuanjia` VALUES ('1', '审批部门—李俊龙', '男', '20', '13888888888', '010000000', 'ljl', 'ljl', 'no');
INSERT INTO `t_zhuanjia` VALUES ('2', '审批部门—陈俊积', '男', '20', '15822222222', '010000000', 'cjj', 'cjj', 'no');
