<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@page import="java.text.SimpleDateFormat" %>

<%
    String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
    <meta http-equiv="description" content="This is my page"/>

    <link rel="stylesheet" type="text/css" href="<%=path %>/css/base.css"/>
    <script type="text/javascript" src="<%=path %>/js/popup.js"></script>
    <script type="text/javascript" src="<%=path %>/js/jquery-1.4.4.min.js"></script>

    <script language="javascript">
        function c() {
            if (document.formAdd.title.value == "") {
                alert("请输入标题");
                return false;
            }
            if (document.formAdd.fujian.value == "") {
                alert("请上传附件");
                return false;
            }

            document.formAdd.submit();
        }

        function up() {
            var pop = new Popup({contentType: 1, isReloadOnClose: false, width: 400, height: 200});
            pop.setContent("contentUrl", "<%=path %>/upload/upload.jsp");
            pop.setContent("title", "文件上传");
            pop.build();
            pop.show();
        }
    </script>
</head>

<body leftmargin="2" topmargin="9" background='<%=path %>/img/allbg.gif'>
<form action="<%=path %>/gaojian?type=gaojianAdd" name="formAdd" method="post">
    <table width="98%" align="center" border="0" cellpadding="4" cellspacing="1" bgcolor="#CBD8AC"
           style="margin-bottom:8px">
        <tr bgcolor="#EEF4EA">
            <td colspan="3" background="<%=path %>/img/wbg.gif" class='title'><span>&nbsp;</span></td>
        </tr>
        <tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';"
            onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
            <td width="10%" bgcolor="#FFFFFF" align="right">
                传输公文名：
            </td>
            <td width="75%" bgcolor="#FFFFFF" align="left">
                <input type="text" name="title" size="90"/>
            </td>
        </tr>
        <tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';"
            onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
            <td width="10%" bgcolor="#FFFFFF" align="right">
                发：
            </td>
            <td width="75%" bgcolor="#FFFFFF" align="left">
                <select id="toUser" name="toUser" placeholder="1">
                    <option value="2">user2</option>
                </select>
            </td>
        </tr>
        <tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';"
            onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
            <td width="10%" bgcolor="#FFFFFF" align="right">
                上传公文：
            </td>
            <td width="75%" bgcolor="#FFFFFF" align="left">
                <input type="text" name="fujian" id="fujian" size="90" readonly="readonly"/>
                <input type="button" value="上传" onclick="up()"/>
                <input type="hidden" name="fujianYuanshiming" id="fujianYuanshiming"/>
            </td>
        </tr>
        <tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';"
            onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
            <td width="10%" bgcolor="#FFFFFF" align="right">
                上传时间：
            </td>
            <td width="75%" bgcolor="#FFFFFF" align="left">
                <input type="text" name="shijian" size="90" readonly="readonly"
                       value="<%=new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date())%>"/>
            </td>
        </tr>
        <tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';"
            onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
            <td width="10%" bgcolor="#FFFFFF" align="right">
                &nbsp;
            </td>
            <td width="75%" bgcolor="#FFFFFF" align="left">
                <input type="button" value="提交" onclick="c()"/>&nbsp;
                <%--                <input id="id1" type="button" value="提交"/>--%>
                <input type="reset" value="重置"/>&nbsp;
            </td>
        </tr>
    </table>
</form>
<script src="<%=path %>/js/myjs.js"></script>
<script type="text/javascript">
    $(function () {
        $.get("<%=path %>/user?type=userManaJson", function (data, status) {
            console.log("数据: " + data + "\n状态: " + status);
            console.log(data)
            let users = JSON.parse(data);
            let dataUser = users.users;
            console.log(dataUser)
            $("#toUser").empty();
            for (let i = 0; i < dataUser.length; i++) {
                console.log(i)
                $("#toUser").append("<option value=" + dataUser[i].loginName + ">" + dataUser[i].name + "</option></br>")
            }
        });
    });
</script>
</body>
</html>
