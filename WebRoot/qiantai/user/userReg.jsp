<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %> 
<%
    String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
	<meta http-equiv="description" content="This is my page"/>
	
	
	<style rel="stylesheet" type="text/css">
		
	</style>
    <script language="javascript">
		    function check1()
		    {
		        if(document.form1.xuehao.value=="")
		        {
		            alert("请输入学号");
		            return false;
		        }
		        if(document.form1.xingming.value=="")
		        {
		            alert("请输入姓名");
		            return false;
		        }
		        if(document.form1.loginpw.value=="")
		        {
		            alert("请输入密码");
		            return false;
		        }
		        document.form1.submit();
		    }
        </script>
  </head>
  
  <body>
      <div id="templatemo_wrapper_outer">
		  <div id="templatemo_wrapper">
			    <jsp:include flush="true" page="/qiantai/inc/incTop.jsp"></jsp:include>
			    
			    
			    <div id="templatemo_content_wrapper" style="margin-top: 10px;">
			         <div id="content">
				          <!-- <div class="section_w610 divider" >
					          <h2 style="padding-top: 10px;">11</h2>
					          <p>
					             	11
							  </p>
				          </div> -->
				          <div class="section_w610" style="padding-top: 10px;">
					          <h2>学生注册</h2>
							  <div style="height: 400px;">
							       <form action="<%=path %>/user?type=userReg" name="form1" method="post">
							       <table align="left" border="0" cellpadding="5" cellspacing="5">
										<tr align='center'>
											<td style="width: 50px;" align="center">
												学号：										    
											</td>
											<td align="left">
												<input name="xuehao" type="text" style="width: 127px;"/>
											</td>
										</tr>
										<tr align='center'>
											<td style="width: 50px;" align="center">
												姓名：										    
											</td>
											<td align="left">
												<input name="xingming" type="text" style="width: 127px;"/>
											</td>
										</tr>
										<tr align='center'>
											<td style="width: 50px;" align="center">
												性别：										    
											</td>
											<td align="left">
												<input type="radio" name="xingbie" value="男" checked="checked" style="border: 0px;"/>男
												&nbsp;&nbsp;&nbsp;
												<input type="radio" name="xingbie" value="女" style="border: 0px;"/>女
											</td>
										</tr>
										<tr align='center'>
											<td style="width: 50px;" align="center">
												年龄：										    
											</td>
											<td align="left">
												<input name="nianling" type="text" style="width: 127px;"/>
											</td>
										</tr>
										<tr align='center'>
											<td style="width: 50px;" align="center">
												班级：										    
											</td>
											<td align="left">
												<input name="banji" type="text" style="width: 127px;"/>
											</td>
										</tr>
										<tr align='center'>
											<td style="width: 50px;" align="center">
												密码：										    
											</td>
											<td align="left">
												<input name="loginpw" type="text" style="width: 127px;" value="000000"/>(默认：000000)
											</td>
										</tr>
										<tr align='center'>
										   <td style="width: 50px;" align="left"></td>
										   <td align="left">
										      <input type="button" value="确定" onclick="check1();"/>
										      <input type="reset" value="重置"/>&nbsp;	
										   </td>
										</tr>
								   </table>
								   </form>
							  </div>
				          </div>
				          <!-- <div class="section_w610">
					          <div class="cleaner"></div>
					          <h2></h2>
					          <div></div>
				          </div> -->
				     </div>
			      
			      
				     <jsp:include flush="true" page="/qiantai/inc/incRight.jsp"></jsp:include>
				     <div class="cleaner"></div>
				</div>
		  </div>
	  </div>
  </body>
</html>
