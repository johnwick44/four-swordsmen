<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %> 

<%
   String path = request.getContextPath();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
      <script type='text/javascript' src='<%=path %>/dwr/interface/loginService.js'></script>
      <script type='text/javascript' src='<%=path %>/dwr/engine.js'></script>
      <script type='text/javascript' src='<%=path %>/dwr/util.js'></script>
      
      <script type="text/javascript">
            function login()
			{                                                                                         
			     if(document.ThisForm.xuehao.value=="")
				 {
				 	alert("请输入用户名");
					return false;
				 }
				 if(document.ThisForm.loginpw.value=="")
				 {
				 	alert("请输入密码");
					return false;
				 }
				 document.getElementById("indicator").style.display="block";
				 loginService.login(document.ThisForm.xuehao.value,document.ThisForm.loginpw.value,2,callback);
			}
		
			function callback(data)
			{
			    document.getElementById("indicator").style.display="none";
			    if(data=="no")
			    {
			        alert("用户名或密码错误");
			    }
			    if(data=="yes")
			    {   alert("登陆成功");
			        window.location.href="<%=path %>/qiantai/default.jsp";
			    }
			    
			}
			
			function zhongxin()
			{
			    var targetWinUrl="<%=path %>/auser/index.jsp";
				var targetWinName="newWin";
				var features="width="+screen.width+" ,height="+screen.height+" ,toolbar=yes, top=0, left=0, menubar=yes, scrollbars=yes, resizable=yes,location=no, status=yes";
				var new_win=window.open(targetWinUrl,targetWinName,features);
			}
      </script>
  </head>
  
  <body>
        <div id="sidebar" style="width: 200px;margin-top: 0px;">
		        <!-- <div class="news_section" style="margin-left:20px;">
			          <div class="news_box">
			            <h3 style="background-color: #CCCCCC"><a href="#">网站日历</a></h3>
			            <p>1111</p>
						<p>2222</p>
			            <div class="date"></div>
			          </div>
		        </div> -->  
		        <div class="news_section" style="margin-left:20px;border:solid 1px #CCCCCC;margin-top: 0px;">
			          <h3 style="background-color: #CCCCCC">学生登录</h3>
			          <div class="news_box">
			              <c:if test="${sessionScope.userType !=2}">
			              <form name="ThisForm" method="POST" action="">
						       <table cellspacing="0" cellpadding="0" width="98%" align="center" border="0">
						          <tr>
						            <td align="center" colspan="2" height="10"></td>
						          </tr>
						          <tr>
						            <td align="right" width="31%" height="30" style="font-size: 11px;">学号：</td>
						            <td align="left" width="69%"><input style="width: 100px;" name="xuehao" type="text" /></td>
						          </tr>
						          <tr>
						            <td align="right" height="30" style="font-size: 11px;">密码：</td>
						            <td align="left"><input type="password" style="width: 100px;" name="loginpw"/></td>
						          </tr>
						          <tr>
						            <td align="center" colspan="2" height="10"></td>
						          </tr>
						          <tr>
						            <td align="center" colspan="2" height="30">
						               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						               <input type="button" value="登  录" onclick="login()" style="border:#ccc 1px solid; background-color:#FFFFFF; font-size:12px; padding-top:3px;" />
									   &nbsp;
									   <input type="reset" value="重   置" style="border:#ccc 1px solid; background-color:#FFFFFF; font-size:12px; padding-top:3px;" />
						               <img id="indicator" src="<%=path %>/img/loading.gif" style="display:none"/>
						            </td>
						          </tr>
						      </table>
					      </form>
					      </c:if>
					      <c:if test="${sessionScope.userType==2}">
				       		    欢迎您：${sessionScope.user.xingming} <br/>
				       		 <a href="#" onclick="zhongxin()">个人中心</a>
				          </c:if>
			          </div>
		        </div> 
		        <div class="news_section" style="margin-left:20px;border:solid 1px #CCCCCC;margin-top: 10px;">
			          <div class="news_box">
			            <jsp:include flush="true" page="/qiantai/rili/rili.jsp"></jsp:include>
			          </div>
		        </div>
	    </div>
  </body>
</html>
