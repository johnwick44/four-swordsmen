<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %> 
<%
    String path = request.getContextPath();
    List goodsList=new ArrayList();
    goodsList.add("a");
    goodsList.add("a");
    goodsList.add("a");
    goodsList.add("a");
    goodsList.add("a");
    
    request.setAttribute("goodsList", goodsList);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
	<meta http-equiv="description" content="This is my page"/>
	
	
	<style rel="stylesheet" type="text/css">
		.list_noimg
		{
		      PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 1px; OVERFLOW: hidden; PADDING-TOP: 1px; BORDER-BOTTOM: #eee 1px solid; ZOOM: 1
		}
		
	</style>
    
  </head>
  
  <body>
      <div id="templatemo_wrapper_outer">
		  <div id="templatemo_wrapper">
			    <jsp:include flush="true" page="/qiantai/inc/incTop.jsp"></jsp:include>
			    
			    
			    <div id="templatemo_content_wrapper" style="margin-top: 10px;">
			         <div id="content">
				          <!-- <div class="section_w610 divider" >
					          <h2 style="padding-top: 10px;">11</h2>
					          <p>
					             	11
							  </p>
				          </div> -->
				          <div class="section_w610" style="padding-top: 10px;">
					          <h2>后勤新闻</h2>
							  <div>
							       <c:forEach items="${requestScope.xinwenList}" var="xinwen" varStatus="s">
							       <DL class=list_noimg><DT><span style="float:left"><a href="<%=path %>/xinwen?type=xinwenDetailQian&id=${xinwen.id}">${xinwen.title}</a></span><span style="float:right">${xinwen.shijian}</span></DT></DL>
							       </c:forEach>
							  </div>
				          </div>
				          <div class="section_w610">
					          <h2>失物招领</h2>
							  <div>
							       <c:forEach items="${requestScope.shiwuList}" var="shiwu" varStatus="s">
							       <DL class=list_noimg><DT><span style="float:left">${shiwu.title}</span><span style="float:right">${shiwu.shijian}</span></DT></DL>
							       </c:forEach>
							  </div>
				          </div>
				          <!-- <div class="section_w610">
					          <div class="cleaner"></div>
					          <h2></h2>
					          <div></div>
				          </div> -->
				     </div>
			      
			      
				     <jsp:include flush="true" page="/qiantai/inc/incRight.jsp"></jsp:include>
				     <div class="cleaner"></div>
				</div>
		  </div>
	  </div>
  </body>
</html>
