package com.action;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.DB;
import com.orm.TAdmin;
import com.orm.Tgaojian;
import com.orm.User;

public class gaojian_servlet extends HttpServlet {
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String type = req.getParameter("type");


        if (type.endsWith("gaojianAdd")) {
            gaojianAdd(req, res);
        }
        if (type.endsWith("gaojianMine")) {
            gaojianMine(req, res);
        }if (type.endsWith("gaojianToMine")) {
            gaojianToMine(req, res);
        }
        if (type.endsWith("gaojianDel")) {
            gaojianDel(req, res);
        }


        if (type.endsWith("gaojianDaishen_zhuanjia")) {
            gaojianDaishen_zhuanjia(req, res);
        }
        if (type.endsWith("gaojianShenhe_zhuanjia")) {
            gaojianShenhe_zhuanjia(req, res);
        }

        if (type.endsWith("gaojianDaishen_admin")) {
            gaojianDaishen_admin(req, res);
        }
        if (type.endsWith("gaojianShenhe_admin")) {
            gaojianShenhe_admin(req, res);
        }

        if (type.endsWith("gaojianRes")) {
            gaojianRes(req, res);
        }

        if (type.endsWith("chaxunRes")) {
            chaxunRes(req, res);
        }
    }


    public void gaojianAdd(HttpServletRequest req, HttpServletResponse res) {
        String title = req.getParameter("title");
        String fujian = req.getParameter("fujian");
        String fujianYuanshiming = req.getParameter("fujianYuanshiming");
        String shijian = req.getParameter("shijian");
        String toUser = req.getParameter("toUser");

        int user_id = ((User) req.getSession().getAttribute("user")).getId();
        String zhuanjiashenhebiaozhi = "审核中";
        String zhuanjiashenheyijian = "";
        String zhubianshenhebiaozhi = "审核中";

        String zhubianshenheyijian = "";

        String sql = "insert into t_gaojian(title,fujian,fujianYuanshiming," +
                "shijian,user_id,zhuanjiashenhebiaozhi,zhuanjiashenheyijian," +
                "zhubianshenhebiaozhi,zhubianshenheyijian,touser) values(?,?,?,?,?,?,?,?,?,?)";
        Object[] params = {title, fujian, fujianYuanshiming,
                shijian, user_id, zhuanjiashenhebiaozhi, zhuanjiashenheyijian,
                zhubianshenhebiaozhi, zhubianshenheyijian, toUser};
        DB mydb = new DB();
        mydb.doPstm(sql, params);
        mydb.closed();

        req.setAttribute("msg", "公文申请上传完毕，等待审核");
        String targetURL = "/common/msg.jsp";
        dispatch(targetURL, req, res);
    }

    // todo tomine
    public void gaojianToMine(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        int user_id = ((User) req.getSession().getAttribute("user")).getId();
        String toUser = ((User) req.getSession().getAttribute("user")).getLoginName();

        List gaojianList = new ArrayList();
        String sql = "select * from t_gaojian where touser=?";
        Object[] params = {toUser};
        DB mydb = new DB();
        try {
            mydb.doPstm(sql, params);
            ResultSet rs = mydb.getRs();
            while (rs.next()) {
                Tgaojian gaojian = new Tgaojian();

                gaojian.setId(rs.getInt("id"));
                gaojian.setTitle(rs.getString("title"));
                gaojian.setFujian(rs.getString("fujian"));
                gaojian.setFujianYuanshiming(rs.getString("fujianYuanshiming"));

                gaojian.setShijian(rs.getString("shijian"));
                gaojian.setUser_id(rs.getInt("user_id"));
                gaojian.setZhuanjiashenhebiaozhi(rs.getString("zhuanjiashenhebiaozhi"));
                gaojian.setzhuanjiashenheyijian(rs.getString("zhuanjiashenheyijian"));

                gaojian.setZhubianshenhebiaozhi(rs.getString("zhubianshenhebiaozhi"));
                gaojian.setZhubianshenheyijian(rs.getString("zhubianshenheyijian"));

                gaojianList.add(gaojian);
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mydb.closed();

        req.setAttribute("gaojianList", gaojianList);
        req.getRequestDispatcher("auser/gaojian/gaojianToMine.jsp").forward(req, res);
    }

    public void gaojianMine(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        int user_id = ((User) req.getSession().getAttribute("user")).getId();

        List gaojianList = new ArrayList();
        String sql = "select * from t_gaojian where user_id=" + user_id;
        Object[] params = {};
        DB mydb = new DB();
        try {
            mydb.doPstm(sql, params);
            ResultSet rs = mydb.getRs();
            while (rs.next()) {
                Tgaojian gaojian = new Tgaojian();

                gaojian.setId(rs.getInt("id"));
                gaojian.setTitle(rs.getString("title"));
                gaojian.setFujian(rs.getString("fujian"));
                gaojian.setFujianYuanshiming(rs.getString("fujianYuanshiming"));

                gaojian.setShijian(rs.getString("shijian"));
                gaojian.setUser_id(rs.getInt("user_id"));
                gaojian.setZhuanjiashenhebiaozhi(rs.getString("zhuanjiashenhebiaozhi"));
                gaojian.setzhuanjiashenheyijian(rs.getString("zhuanjiashenheyijian"));

                gaojian.setZhubianshenhebiaozhi(rs.getString("zhubianshenhebiaozhi"));
                gaojian.setZhubianshenheyijian(rs.getString("zhubianshenheyijian"));

                gaojianList.add(gaojian);
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mydb.closed();

        req.setAttribute("gaojianList", gaojianList);
        req.getRequestDispatcher("auser/gaojian/gaojianMine.jsp").forward(req, res);
    }

    public void gaojianDel(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String sql = "delete from t_gaojian where id=" + Integer.parseInt(req.getParameter("id"));
        Object[] params = {};
        DB mydb = new DB();
        mydb.doPstm(sql, params);
        mydb.closed();

        req.setAttribute("msg", "公文申请删除完毕");
        req.getRequestDispatcher("common/msg.jsp").forward(req, res);
    }


    public void gaojianDaishen_zhuanjia(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        List gaojianList = new ArrayList();
        String sql = "select * from t_gaojian where zhuanjiashenhebiaozhi='审核中'";
        Object[] params = {};
        DB mydb = new DB();
        try {
            mydb.doPstm(sql, params);
            ResultSet rs = mydb.getRs();
            while (rs.next()) {
                Tgaojian gaojian = new Tgaojian();

                gaojian.setId(rs.getInt("id"));
                gaojian.setTitle(rs.getString("title"));
                gaojian.setFujian(rs.getString("fujian"));
                gaojian.setFujianYuanshiming(rs.getString("fujianYuanshiming"));

                gaojian.setShijian(rs.getString("shijian"));
                gaojian.setUser_id(rs.getInt("user_id"));
                gaojian.setZhuanjiashenhebiaozhi(rs.getString("zhuanjiashenhebiaozhi"));
                gaojian.setzhuanjiashenheyijian(rs.getString("zhuanjiashenheyijian"));

                gaojian.setZhubianshenhebiaozhi(rs.getString("zhubianshenhebiaozhi"));
                gaojian.setZhubianshenheyijian(rs.getString("zhubianshenheyijian"));

                gaojianList.add(gaojian);
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mydb.closed();

        req.setAttribute("gaojianList", gaojianList);
        req.getRequestDispatcher("azhuanjia/gaojian/gaojianDaishen_zhuanjia.jsp").forward(req, res);
    }


    public void gaojianShenhe_zhuanjia(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String zhuanjiashenhebiaozhi = req.getParameter("zhuanjiashenhebiaozhi");
        String zhuanjiashenheyijian = req.getParameter("zhuanjiashenheyijian");

        String sql = "update t_gaojian set zhuanjiashenhebiaozhi=?,zhuanjiashenheyijian=? where id=" + Integer.parseInt(req.getParameter("id"));
        Object[] params = {zhuanjiashenhebiaozhi, zhuanjiashenheyijian};
        DB mydb = new DB();
        mydb.doPstm(sql, params);
        mydb.closed();

        req.setAttribute("msg", "公文申请审核完毕");
        req.getRequestDispatcher("common/msg.jsp").forward(req, res);
    }


    public void gaojianDaishen_admin(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        List gaojianList = new ArrayList();
        String sql = "select * from t_gaojian where zhuanjiashenhebiaozhi='ͨ通过' and zhubianshenhebiaozhi='审核中'";
        Object[] params = {};
        DB mydb = new DB();
        try {
            mydb.doPstm(sql, params);
            ResultSet rs = mydb.getRs();
            while (rs.next()) {
                Tgaojian gaojian = new Tgaojian();

                gaojian.setId(rs.getInt("id"));
                gaojian.setTitle(rs.getString("title"));
                gaojian.setFujian(rs.getString("fujian"));
                gaojian.setFujianYuanshiming(rs.getString("fujianYuanshiming"));

                gaojian.setShijian(rs.getString("shijian"));
                gaojian.setUser_id(rs.getInt("user_id"));
                gaojian.setZhuanjiashenhebiaozhi(rs.getString("zhuanjiashenhebiaozhi"));
                gaojian.setzhuanjiashenheyijian(rs.getString("zhuanjiashenheyijian"));

                gaojian.setZhubianshenhebiaozhi(rs.getString("zhubianshenhebiaozhi"));
                gaojian.setZhubianshenheyijian(rs.getString("zhubianshenheyijian"));

                gaojianList.add(gaojian);
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mydb.closed();

        req.setAttribute("gaojianList", gaojianList);
        req.getRequestDispatcher("admin/gaojian/gaojianDaishen_admin.jsp").forward(req, res);
    }


    public void gaojianShenhe_admin(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String zhubianshenhebiaozhi = req.getParameter("zhubianshenhebiaozhi");
        String zhubianshenheyijian = req.getParameter("zhubianshenheyijian");

        String sql = "update t_gaojian set zhubianshenhebiaozhi=?,zhubianshenheyijian=? where id=" + Integer.parseInt(req.getParameter("id"));
        Object[] params = {zhubianshenhebiaozhi, zhubianshenheyijian};
        DB mydb = new DB();
        mydb.doPstm(sql, params);
        mydb.closed();

        req.setAttribute("msg", "公文申请审核完毕");
        req.getRequestDispatcher("common/msg.jsp").forward(req, res);
    }

    public void gaojianRes(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String title = req.getParameter("title").trim();

        List gaojianList = new ArrayList();
        String sql = "select * from t_gaojian where title like '%" + title + "%'" + " and zhuanjiashenhebiaozhi='ͨ通过' and zhubianshenhebiaozhi='ͨ通过'";
        Object[] params = {};
        DB mydb = new DB();
        try {
            mydb.doPstm(sql, params);
            ResultSet rs = mydb.getRs();
            while (rs.next()) {
                Tgaojian gaojian = new Tgaojian();

                gaojian.setId(rs.getInt("id"));
                gaojian.setTitle(rs.getString("title"));
                gaojian.setFujian(rs.getString("fujian"));
                gaojian.setFujianYuanshiming(rs.getString("fujianYuanshiming"));

                gaojian.setShijian(rs.getString("shijian"));
                gaojian.setUser_id(rs.getInt("user_id"));
                gaojian.setZhuanjiashenhebiaozhi(rs.getString("zhuanjiashenhebiaozhi"));
                gaojian.setzhuanjiashenheyijian(rs.getString("zhuanjiashenheyijian"));

                gaojian.setZhubianshenhebiaozhi(rs.getString("zhubianshenhebiaozhi"));
                gaojian.setZhubianshenheyijian(rs.getString("zhubianshenheyijian"));

                gaojianList.add(gaojian);
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mydb.closed();

        req.setAttribute("gaojianList", gaojianList);
        req.getRequestDispatcher("admin/gaojian/gaojianRes.jsp").forward(req, res);
    }


    public void chaxunRes(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String title = req.getParameter("title").trim();

        List gaojianList = new ArrayList();
        String sql = "select * from t_gaojian where title like '%" + title + "%'" + " and zhuanjiashenhebiaozhi='ͨ通过' and zhubianshenhebiaozhi='ͨ通过'";
        Object[] params = {};
        DB mydb = new DB();
        try {
            mydb.doPstm(sql, params);
            ResultSet rs = mydb.getRs();
            while (rs.next()) {
                Tgaojian gaojian = new Tgaojian();

                gaojian.setId(rs.getInt("id"));
                gaojian.setTitle(rs.getString("title"));
                gaojian.setFujian(rs.getString("fujian"));
                gaojian.setFujianYuanshiming(rs.getString("fujianYuanshiming"));

                gaojian.setShijian(rs.getString("shijian"));
                gaojian.setUser_id(rs.getInt("user_id"));
                gaojian.setZhuanjiashenhebiaozhi(rs.getString("zhuanjiashenhebiaozhi"));
                gaojian.setzhuanjiashenheyijian(rs.getString("zhuanjiashenheyijian"));

                gaojian.setZhubianshenhebiaozhi(rs.getString("zhubianshenhebiaozhi"));
                gaojian.setZhubianshenheyijian(rs.getString("zhubianshenheyijian"));

                gaojianList.add(gaojian);
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mydb.closed();

        req.setAttribute("gaojianList", gaojianList);
        req.getRequestDispatcher("azhuanjia/gaojian/chaxunRes.jsp").forward(req, res);
    }


    public void dispatch(String targetURI, HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatch = getServletContext().getRequestDispatcher(targetURI);
        try {
            dispatch.forward(request, response);
            return;
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void destroy() {

    }
}
