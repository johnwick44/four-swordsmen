package com.orm;

public class Tgaojian
{
	private int id;
	private String title;
	private String fujian;
	private String fujianYuanshiming;
	
	private String shijian;
	private int user_id;
	private String zhuanjiashenhebiaozhi;//通过-未过--审核中
	private String zhuanjiashenheyijian;
	
	private String zhubianshenhebiaozhi;//通过-未过--审核中
	private String zhubianshenheyijian;
	
	
	
	public String getFujian()
	{
		return fujian;
	}
	public void setFujian(String fujian)
	{
		this.fujian = fujian;
	}
	public String getFujianYuanshiming()
	{
		return fujianYuanshiming;
	}
	public void setFujianYuanshiming(String fujianYuanshiming)
	{
		this.fujianYuanshiming = fujianYuanshiming;
	}
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	
	public String getShijian()
	{
		return shijian;
	}
	public void setShijian(String shijian)
	{
		this.shijian = shijian;
	}
	public String getTitle()
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	public int getUser_id()
	{
		return user_id;
	}
	public void setUser_id(int user_id)
	{
		this.user_id = user_id;
	}
	public String getZhuanjiashenhebiaozhi()
	{
		return zhuanjiashenhebiaozhi;
	}
	public void setZhuanjiashenhebiaozhi(String zhuanjiashenhebiaozhi)
	{
		this.zhuanjiashenhebiaozhi = zhuanjiashenhebiaozhi;
	}
	public String getzhuanjiashenheyijian()
	{
		return zhuanjiashenheyijian;
	}
	public void setzhuanjiashenheyijian(String zhuanjiashenheyijian)
	{
		this.zhuanjiashenheyijian = zhuanjiashenheyijian;
	}
	public String getZhubianshenhebiaozhi()
	{
		return zhubianshenhebiaozhi;
	}
	public void setZhubianshenhebiaozhi(String zhubianshenhebiaozhi)
	{
		this.zhubianshenhebiaozhi = zhubianshenhebiaozhi;
	}
	public String getZhubianshenheyijian()
	{
		return zhubianshenheyijian;
	}
	public void setZhubianshenheyijian(String zhubianshenheyijian)
	{
		this.zhubianshenheyijian = zhubianshenheyijian;
	}
	
	
}
