package com.orm;

public class User
{
	private int id;

	private String name;

	private String sex;

	private String age;

	private String tel;

	private String address;


	private String loginName;

	private String loginPw;

	private String toUser;

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}


	public String getAge()
	{
		return age;
	}

	public void setAge(String age)
	{
		this.age = age;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getLoginName()
	{
		return loginName;
	}

	public void setLoginName(String loginName)
	{
		this.loginName = loginName;
	}

	public String getLoginPw()
	{
		return loginPw;
	}

	public void setLoginPw(String loginPw)
	{
		this.loginPw = loginPw;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}


	public String getSex()
	{
		return sex;
	}

	public void setSex(String sex)
	{
		this.sex = sex;
	}

	public String getTel()
	{
		return tel;
	}

	public void setTel(String tel)
	{
		this.tel = tel;
	}

	@Override
	public String toString() {
		return "{" +
				"\"id\":\"" + id + '\"' +
				", \"name\":\"" + name + '\"' +
				", \"sex\":\"" + sex + '\"' +
				", \"age\":\"" + age + '\"' +
				", \"tel\":\"" + tel + '\"' +
				", \"address\":\"" + address + '\"' +
				", \"loginName\":\"" + loginName + '\"' +
				", \"loginPw\":\"" + loginPw + '\"' +
				'}';
	}
}
